import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FoodOrderingMachine {
    private JPanel panel1;
    private JPanel root;
    private JLabel topLabel;
    private JButton sushiButton;
    private JButton udonButton;
    private JButton sukiyakiButton;
    private JButton yakizakanaButton;
    private JButton okonomiyakiButton;
    private JButton tempuraButton;
    private JButton checkOutButton;
    private JButton randomButton;
    private JTextArea totalTextArea;
    private JTextArea orderListTextArea;
    double total = 0;

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            int confirmation2 = JOptionPane.showConfirmDialog(null,
                    "Would you like to make it a large? The portion will be 1.5 times larger and the price will be 1.2 times higher.",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
            String currentText = orderListTextArea.getText();
            if (confirmation2 == 0) {
                JOptionPane.showMessageDialog(null, "Ordered for large " + food + " received.");
                total += price * 1.2;
                orderListTextArea.setText(currentText + "large " + food + " " + price * 1.2 + "yen\n");
                totalTextArea.setText("Total:" + total + "yen");
            } else {
                JOptionPane.showMessageDialog(null, "Ordered for " + food + " received.");
                total += price;
                orderListTextArea.setText(currentText + food + " " + price + "yen\n");
                totalTextArea.setText("Total:" + total + "yen");
            }
        }

    }

    void randomOrder(double budget) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to place random orders within the budget of " + budget + " yen?",
                "Random Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == JOptionPane.YES_OPTION) {
            List<String> foods = List.of("Sushi", "Sukiyaki", "Okonomiyaki", "Udon", "Yakizakana", "Tempura");
            List<Integer> prices = List.of(2750, 3300, 880, 770, 1320, 1760);
            List<String> orderedFoods = new ArrayList<>();
            double currentTotal = 0;
            Random random = new Random();

            while (currentTotal < budget) {
                int index = random.nextInt(foods.size());
                String food = foods.get(index);
                int price = prices.get(index);
                boolean isLarge = random.nextBoolean();
                double actualPrice = isLarge ? price * 1.2 : price;

                if (currentTotal + actualPrice <= budget) {
                    orderedFoods.add(food + (isLarge ? " (Large)" : ""));
                    currentTotal += actualPrice;
                } else if (isLarge && currentTotal + price <= budget) {
                    orderedFoods.add(food);
                    currentTotal += price;
                } else {
                    // If we can't afford the current selection, try others
                    boolean affordable = false;
                    for (int i = 0; i < foods.size(); i++) {
                        double testPrice = random.nextBoolean() ? prices.get(i) * 1.2 : prices.get(i);
                        if (currentTotal + testPrice <= budget) {
                            index = i;
                            food = foods.get(index);
                            price = prices.get(index);
                            isLarge = testPrice > prices.get(index);
                            actualPrice = testPrice;
                            affordable = true;
                            break;
                        }
                    }
                    if (!affordable) {
                        break; // Exit loop if no more items can be afforded
                    }
                }
            }

            if (orderedFoods.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Unable to order within the given budget.");
            } else {
                for (String food : orderedFoods) {
                    boolean isLarge = food.contains("(Large)");
                    double finalPrice = isLarge ? prices.get(foods.indexOf(food.replace(" (Large)", ""))) * 1.2 : prices.get(foods.indexOf(food.replace(" (Large)", "")));

                    String currentText = orderListTextArea.getText();
                    total += finalPrice;
                    orderListTextArea.setText(currentText + food + " " + finalPrice + " yen\n");
                    totalTextArea.setText("Total: " + total + " yen");
                }
                JOptionPane.showMessageDialog(null, "Random orders placed: \n" + String.join(", ", orderedFoods) + "\nTotal price: " + currentTotal + " yen.");
            }
        }
    }

    public FoodOrderingMachine() {
        sushiButton.setIcon(new ImageIcon(this.getClass().getResource("Sushi.png")));
        sukiyakiButton.setIcon(new ImageIcon(this.getClass().getResource("Sukiyaki.png")));
        okonomiyakiButton.setIcon(new ImageIcon(this.getClass().getResource("Okonomiyaki.png")));
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("Udon.png")));
        yakizakanaButton.setIcon(new ImageIcon(this.getClass().getResource("Yakizakana.png")));
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("Tempura.png")));

        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",2750);
            }
        });
        sukiyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sukiyaki",3850);
            }
        });
        okonomiyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Okonomiyaki",880);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",770);
            }
        });
        yakizakanaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakizakana",1320);
            }
        });
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",1760);
            }
        });


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0){
                     JOptionPane.showMessageDialog(null,"Thank you. The total price is " + total + "yen.");
                     orderListTextArea.setText("");
                     total = 0;
                     totalTextArea.setText("Total: 0 yen");
                }
            }
        });
        randomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String budgetStr = JOptionPane.showInputDialog(null, "Enter your budget in yen:");
                if (budgetStr != null && !budgetStr.isEmpty()) {
                    try {
                        double budget = Double.parseDouble(budgetStr);
                        randomOrder(budget);
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Invalid input. Please enter a numeric value.");
                    }
                }
            }
        });

        totalTextArea.setText("Total: 0 yen");
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
